---
layout: handbook-page-toc
title: "Talent Development Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Why a Talent Development Program is vital for GitLab

"A key cost is loss of talent. There is a huge cost that goes beyong direct dollars. Hi-potentials are a scarse resource, and we're tough on them. If they don't make it, you've washed out a high-potential." Source: The First 90 Days, by Michael D. Watkins.

As we continue to scale, we want to be intentional about Talent Development. Investing in a Talent Development Program creates the opportunity for GitLab to build diverse, flexible and versatile teams, as well as of the utmost importance to ongoing success across all teams.

We are using this [Talent Development Program issue](https://gitlab.com/gitlab-com/people-group/General/-/issues/719) to work on the content and overall planning of this page and the program. Follow along and please contribute!  



