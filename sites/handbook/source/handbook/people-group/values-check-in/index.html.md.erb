---
layout: handbook-page-toc
title: "Values Check In"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Values Check-In Survey

GitLab's [values](https://about.gitlab.com/handbook/values/) are a crucial part of our [competencies list](https://about.gitlab.com/handbook/competencies/#list) and are considered essential for team members to learn and live out, irrespective of role or team. To ensure all team members are living out and practicing GitLab's values, we have implemented a Values Check-In survey for new team members and their managers after 8-10 weeks with the company. The purpose of the check-in is to ensure alignment from a performance perspective and understanding of expectations with regard to the values.

Both team members and their managers will take the survey on the team member's behalf. After both parties have completed the survey, results and feedback should be discussed in the next 1:1 session.

The aim of this check-in is to ensure continuous improvment through analysis of trends and feedback, with a positive side-effect that will attribute to great retention and values alignment.

### Manager Check-In

When you receive the dedicated email from the People Experience team, complete the Manager Values Check-In form for the relevant team member. When you submit your form, you will also receive a receipt of your submissions. Communicate with your team member that you have completed your review and that you plan to discuss both your and their results at a date prior to the deadline. At this 1:1, or prior to it, share your results and ask your team member to also share their results with you. The purpose of the check-in is to ensure alignment from a performance perspective and understanding of expectations with regard to the values. Feel free to contact the People Specialist team (`peopleops@domain`) if you have any questions or concerns.

### Team Member Check-In

When you receive the dedicated email from the People Experience team, complete the Team Member Values Check-In form. When you submit your form, you will also receive a receipt of your submissions. Communicate with your manager that you have completed your review. Your manager will coordinate a 1:1 to discuss both your and their results. At this 1:1, or prior to it, share your results and remind your manager to also share their results with you. The purpose of the check-in is to ensure alignment from a performance perspective and understanding of expectations with regard to the values. Feel free to contact the People Specialist team (`peopleops@domain`) if you have any questions or concerns.

### Timeline

We will take [probation periods](https://about.gitlab.com/handbook/people-group/contracts-and-international-expansion/#probation-period) into account when considering the timing of the check in. Team members and managers will have ~_2 weeks_ to complete the survey once it is received. It is essential that the survey is completely in a timely fashion, especially for countries that have a probation period implemented.

| ***Probation Period Length***           | ***Survey Sent***  | ***Survey Completion Deadline*** |
|-----------------------------------|-----------------|-----------------|
| 1 month | 8 weeks* | 10 weeks*
| 3 months | 6 weeks | 8 weeks
| 6 months | 8 weeks  | 10 weeks
| _No probationary period_ | 8 weeks  | 10 weeks

_*Considering that team members are still [onboarding](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#overview) during their first 30 days with GitLab, team members in countries that follow a 1-month probation period will not have their Values Check-In until after 8 weeks at the company._

## [Team Member](https://docs.google.com/forms/d/e/1FAIpQLSd71MxvRaBjhaxSiFW0qo0blULu9jQ0ypkU7zPEU3p-IimpIQ/viewform) Survey Questions

#### Personal Information

_Question format: open-ended_

* What is your name?
* What is your role?
* What is your manager's name?

#### General

_Question format: Likert scale_

* Since you have started with GitLab, you have developed an understanding of our values.
* GitLab provides an environment & tools to fully showcase its values.
* Your manager & teammates help you to display GitLab's values.

#### Collaboration

_Question format: Likert scale & open-ended_

* During the first 8-10 weeks at GitLab, I have displayed the Value of Collaboration.
* Provide an example of where you have displayed the value of Collaboration.

#### Results

_Question format: Likert scale & open-ended_

* During the first 8-10 weeks at GitLab, I have displayed the Value of Results.
* Provide an example of where you have displayed the value of Results.

#### Efficiency

_Question format: Likert scale & open-ended_

* During the first 8-10 weeks at GitLab, I have displayed the Value of Efficiency.
* Provide an example of where you have displayed the value of Efficiency.

#### Diversity, Inclusion, & Belonging

_Question format: Likert scale & open-ended_

* During the first 8-10 weeks at GitLab, I have displayed the Value of Diversity, Inclusion, & Belonging.
* Provide an example of where you have displayed the value of Diversity, Inclusion, & Belonging.

### Iteration

_Question format: Likert scale & open-ended_

* During the first 8-10 weeks at GitLab, I have displayed the Value of Iteration.
* Provide an example of where you have displayed the value of Iteration.

### Transparency

_Question format: Likert scale & open-ended_

* During the first 8-10 weeks at GitLab, I have displayed the Value of Transparency.
* Provide an example of where you have displayed the value of Transparency.

### Strengths & Improvement Areas

_Question format: Checkboxes_

* Which value(s) do you feel require further focus from you?
* Which value(s) do you feel you have displayed the most?


## [Manager](https://docs.google.com/forms/d/e/1FAIpQLSfo1OVq-sg2mGu19Nd_fylegKe0068CWfIFN9B8ILjZzlPqow/viewform) Survey Questions

#### Personal Information

_Question format: open-ended_

* What is your name?
* What is your role?
* What is your team member's name?

#### Collaboration

_Question format: Likert scale_

* During your new team member's first 8-10 weeks at GitLab, they have displayed the value of Collaboration.

#### Results

_Question format: Likert scale_

* During your new team member's first 8-10 weeks at GitLab, they have displayed the value of Results.

#### Efficiency

_Question format: Likert scale_

* During your new team member's first 8-10 weeks at GitLab, they have displayed the value of Efficiency.

#### Diversity, Inclusion, & Belonging

_Question format: Likert scale_

* During your new team member's first 8-10 weeks at GitLab, they have displayed the value of Diversity, Inclusion, and Belonging.

#### Iteration

_Question format: Likert scale_

* During your new team member's first 8-10 weeks at GitLab, they have displayed the value of Iteration.

#### Transparency

_Question format: Likert scale_

* During your new team member's first 8-10 weeks at GitLab, they have displayed the value of Transparency.

#### Moving Forward

_Question format: Likert scale, multiple choice, open-ended_

* Overall, you are satisfied with this team member's performance.
* If a probationary period is applicable, you would be happy to pass this team member?
* If applicable, please provide additional context.

## People Experience Team Process

There's an automation setup to check daily who needs to receive the check-in email. You can find more information about it [here](/handbook/people-group/engineering/email-automation/#values-check-in-email).
