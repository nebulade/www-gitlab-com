---
layout: handbook-page-toc
title: Zendesk Schedules
---

# Zendesk Schedules

Schedules arein Zendesk are like schedules in most other things: windows of
time. We use these to determine business hours and various regional working
hours.

## GitLab Schedules

### [Business Hours](https://gitlab.zendesk.com/agent/admin/schedules/91387)

* Timezone: UTC
* Sunday: Closed
* Monday: 0300-0000
* Tuesday: 0000-0000
* Wednesday: 0000-0000
* Thursday: 0000-0000
* Friday: 0000-0000
* Saturday: Closed

### [EMEA](https://gitlab.zendesk.com/agent/admin/schedules/360000029879)

* Timezone: UTC
* Sunday: Closed
* Monday: 0700-1700
* Tuesday: 0700-1700
* Wednesday: 0700-1700
* Thursday: 0700-1700
* Friday: 0700-1700
* Saturday: Closed

### [AMER](https://gitlab.zendesk.com/agent/admin/schedules/360000029899)

* Timezone: UTC
* Sunday: Closed
* Monday: 1300-0000
* Tuesday: 0000-0100, 1300-0000
* Wednesday: 0000-0100, 1300-0000
* Thursday: 0000-0100, 1300-0000
* Friday: 0000-0100, 1300-0000
* Saturday: Closed

### [APAC](https://gitlab.zendesk.com/agent/admin/schedules/360000029919)

* Timezone: UTC
* Sunday: 2200-0000
* Monday: 0000-1100, 2200-0000
* Tuesday: 0000-1100, 2200-0000
* Wednesday: 0000-1100, 2200-0000
* Thursday: 0000-1100, 2200-0000
* Friday: 0000-1100
* Saturday: Closed

### [Low Priority Tickets](https://gitlab.zendesk.com/agent/admin/schedules/360000044539)

* Timezone: UTC
* Sunday: Closed
* Monday: 0300-0000
* Tuesday: 0000-0000
* Wednesday: 0000-0000
* Thursday: 0000-0000
* Friday: 0000-0000
* Saturday: Closed