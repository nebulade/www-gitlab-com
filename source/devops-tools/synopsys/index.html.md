---
layout: markdown_page
title: "Synopsys"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary

Synopsys has separate products for each type of application security testing. They include the following.

* BlackDuck for dependency scanning, container scanning, and license management.  Details are under BlackDuck         comparison.

* Coverity for SAST Coverity includes spell-checker-like capability with an IDE plug-in that alerts the               developer to vulnerable phrases as they code. It also has dashboard that pulls in IAST from Seeker for a            unified view. Coverity covers 20 programming languages. Our research indicates that Coverity costs around           $12k USD for 5 users for the year.

* Seeker for IAST Seeker employs and agent on the application. It is used during functional testing so that           security tests are done in the normal course of other testing. Seekers has an API to integrate with Dev             IDE. Seeker works with Java/all JVM languages. Seeker is only available on prem.

GitLab Ultimate automatically includes broad security scanning with every code commit including Static and Dynamic Application Security Testing, along with dependency scanning, container scanning, and license management. Results are provided to the developer in the CI pipeline (Merge Request) with no integration required. While Synopsys can integrate with IDE's and DevOps tools via the API, that approach will require multiple software licenses and integration / maintenance effort.


## GitLab SAST Benchmarks
  * About this Benchmark
     * This comparison was run by the GitLab Security Research team.
     * GitLab dynamically identifies the [language used](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks) and applies the appropriate SAST scanner. This study used the Java SAST scanner against a Java app.
    * The complete benchmark findings can be found here [GitLab SAST Benchmarks](https://gitlab-org.gitlab.io/-/security-products/sast-benchmark/-/jobs/480175100/artifacts/public/stats.html)

![GitLab SAST Benchmarks](/devops-tools/synopsys/Benchmark_graph.png)

* GitLab and Coverity had very similar results. GitLab found slightly fewer false positives and slightly more true positives.  
* While this represents only one scanner, we expect results to be similar for other languages given the widespread use of the open source scanners that power our SAST testing.  
* GitLab security scanning includes not only SAST but also DAST, Container and Dependency scanning, License Compliance scanning, and Secrets detection. All of these are included in GitLab Ultimate and integrated directly into the developer's workflow.
* Finding vulnerabilities is only the beginning. Delivering those findings to the developer for immediate remediation is key to shifting left to reduce both cost and risk.

## Comparison
